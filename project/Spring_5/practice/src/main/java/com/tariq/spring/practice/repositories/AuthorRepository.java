package com.tariq.spring.practice.repositories;

import com.tariq.spring.practice.model.Author;
import org.springframework.data.repository.CrudRepository;

public interface AuthorRepository extends CrudRepository<Author, Long>{
}
