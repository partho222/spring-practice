package com.tariq.spring.practice.repositories;

import com.tariq.spring.practice.model.Publisher;
import org.springframework.data.repository.CrudRepository;

public interface PublisherRepository extends CrudRepository<Publisher, Long> {
}
