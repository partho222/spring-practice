package com.tariq.spring.practice.bootstrap;

import com.tariq.spring.practice.repositories.AuthorRepository;
import com.tariq.spring.practice.repositories.BookRepository;
import com.tariq.spring.practice.repositories.PublisherRepository;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

/* to declare a spring bean we need to add '@Component' annotation
* Spring bean will enable auto-wiring feature */

/* we need to implement ApplicationListener for binding with
 * application context */
@Component
public class DevBootStrap implements ApplicationListener<ContextRefreshedEvent>{

    /* need to add model repository to enable data/DB
     * access layer CRUD operations  */
    private AuthorRepository authorRepository;
    private BookRepository bookRepository;
    private PublisherRepository publisherRepository;

    /* to use auto-wiring facility we constructor for
    * dependency injection */
    public DevBootStrap(AuthorRepository authorRepository, BookRepository bookRepository, PublisherRepository publisherRepository) {
        this.authorRepository = authorRepository;
        this.bookRepository = bookRepository;
        this.publisherRepository = publisherRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        initData();
    }

    private void initData() {

    }
}
